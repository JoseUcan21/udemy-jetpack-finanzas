package mx.jossprogramming.demofinanzascompose

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DemoFinanzasApplication:Application()