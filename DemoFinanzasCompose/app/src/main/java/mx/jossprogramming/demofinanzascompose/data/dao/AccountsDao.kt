package mx.jossprogramming.demofinanzascompose.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import mx.jossprogramming.demofinanzascompose.data.entitys.AccountEntity
import mx.jossprogramming.demofinanzascompose.data.entitys.AccountWithTransactions

@Dao
interface AccountsDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertAccount(account:AccountEntity)

    @Update
    suspend fun editAccount(account:AccountEntity)

    @Delete
    suspend fun deleteAccount(account: AccountEntity)

    @Transaction
    @Query("SELECT * FROM AccountEntity WHERE idAccount = :idAccount")
    suspend fun getAccountWithTransactions(idAccount:Long):AccountWithTransactions
}