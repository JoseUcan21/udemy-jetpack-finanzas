package mx.jossprogramming.demofinanzascompose.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import mx.jossprogramming.demofinanzascompose.data.entitys.TransactionsEntity

@Dao
interface TransactionsDao {
    @Insert
    suspend fun insertTransaction(transactionsEntity: TransactionsEntity)

    @Update
    suspend fun editTransaction(transactionsEntity: TransactionsEntity)

    @Delete
    suspend fun deleteTransaction(transactionsEntity: TransactionsEntity)

    @Query("SELECT * FROM TransactionsEntity")
    suspend fun getTransactions():List<TransactionsEntity>
}