package mx.jossprogramming.demofinanzascompose.data.entitys

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.util.Date

@Entity
data class AccountEntity(
    @PrimaryKey(autoGenerate = true)
    val idAccount:Long,
    var nameAccount:String,
)

@Entity
data class TransactionsEntity(
    @PrimaryKey(autoGenerate = true)
    val idTransaction:Long,
    val accountId:Long,
    var amount:Double,
    var description:String,
    var dateTransaction: Date,
)

data class AccountWithTransactions(
    @Embedded val account:AccountEntity,
    @Relation(
        parentColumn = "idAccount",
        entityColumn = "accountId"
    )
    val transactionsList:List<TransactionsEntity>
)
