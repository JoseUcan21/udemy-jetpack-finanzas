package mx.jossprogramming.demofinanzascompose.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import mx.jossprogramming.demofinanzascompose.data.converters.Converters
import mx.jossprogramming.demofinanzascompose.data.dao.AccountsDao
import mx.jossprogramming.demofinanzascompose.data.dao.TransactionsDao
import mx.jossprogramming.demofinanzascompose.data.entitys.AccountEntity
import mx.jossprogramming.demofinanzascompose.data.entitys.TransactionsEntity

@Database(entities = [AccountEntity::class,TransactionsEntity::class],version = 1)
@TypeConverters(Converters::class)
abstract class FinanzasDatabase:RoomDatabase() {
    abstract fun accountsDao():AccountsDao
    abstract fun transactionsDao():TransactionsDao
}