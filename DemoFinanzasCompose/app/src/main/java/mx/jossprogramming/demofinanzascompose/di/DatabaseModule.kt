package mx.jossprogramming.demofinanzascompose.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import mx.jossprogramming.demofinanzascompose.data.dao.AccountsDao
import mx.jossprogramming.demofinanzascompose.data.dao.TransactionsDao
import mx.jossprogramming.demofinanzascompose.data.database.FinanzasDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun provideAccountDao(database: FinanzasDatabase):AccountsDao{
        return database.accountsDao()
    }

    @Provides
    @Singleton
    fun provideTransactionsDao(database: FinanzasDatabase):TransactionsDao{
        return database.transactionsDao()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context):FinanzasDatabase{
        return Room.databaseBuilder(
            appContext,
            FinanzasDatabase::class.java,
            "Finanzas Database"
        ).build()
    }
}