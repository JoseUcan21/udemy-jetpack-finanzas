package mx.jossprogramming.demofinanzascompose.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import mx.jossprogramming.demofinanzascompose.data.dao.AccountsDao
import mx.jossprogramming.demofinanzascompose.data.dao.TransactionsDao
import javax.inject.Inject

@HiltViewModel
class FinanzasViewModel @Inject constructor(
    private val accountDao: AccountsDao,
    private val transactionsDao: TransactionsDao
)
    :ViewModel()
{
}